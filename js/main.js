let slideIndex = 1;
showSlides(slideIndex);

let prev = document.getElementById('prev');
let next = document.getElementById('next');

function currentSlide(n) {
   showSlides(slideIndex = n);
}

function showSlides(n) {
   let slides = document.getElementsByClassName("myslide");
   if (n > slides.length) {
      slideIndex = 1;
   }
   if (n < 1) {
      slideIndex = slides.length;
   }

   for (let slide of slides) {
      slide.style.display = "none";
   }
   slides[slideIndex - 1].style.display = "flex";
}
var timer = 0;
makeTimer();
function makeTimer() {
   clearInterval(timer)
   timer = setInterval(function () {
      slideIndex++;
      showSlides(slideIndex);
   }, 5000);
}

var seconds = 5;
setInterval(() => {
   seconds = seconds - 1;
   if (!seconds) { seconds = 5; }
}, 1000)

class Login {
   constructor() {
      this.login = document.getElementById("login")
      this.pass = document.getElementById("pass")
      this.closeForm()
      this.viewForm()
      this.userVerification()
   }

   userVerification() {
      const form = document.querySelector('.modal')
      const button = document.querySelector('.modal-text-button')
      let userLogin
      let userPass

      this.login.addEventListener('input', () => {
         userLogin = this.login.value
      })
      this.pass.addEventListener('input', () => {
         userPass = this.pass.value
      })

      let valid = (email = userLogin) => {
         var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
         return re.test(String(email).toLowerCase());
      }

      button.onclick = (event) => {
         event.stopImmediatePropagation()
         let checkLogin = false
         let checkPass = false

         if (userLogin === undefined || userLogin === null || userLogin === "" || !valid()) {
            this.login.style.background = "yellow"
            checkLogin = false
            alert("Логин не верный. Введите правильно свой почтовый адрес")
         }
         else {
            checkLogin = true
            this.login.style.background = "white"
         }


         if (userPass === undefined || userPass === null || userPass.length < 6 || userPass === "") {
            checkPass = false
            this.pass.style.background = "yellow"
            alert("Пароль не может быть пустым или меньше 6 символов")
         }
         else {
            checkPass = true
            this.pass.style.background = "white"
         }

         if (checkLogin === true && checkPass === true) {
            console.log("close")

            form.style.opacity = "0";
            form.style.transition = "opacity 2s";

            setTimeout(() => {
               form.style = 0
            }, 2000);
         }

         console.log(`${valid()}`)
         console.log(`click userLogin - ${userLogin} userPass  - ${userPass}`)

      }
      return form
   }

   closeForm() {
      const form = document.querySelector('.modal')
      const close = document.querySelector('.modal-close-button')

      close.onclick = (e) => {
         e.stopImmediatePropagation()
         console.log("close")

         form.style.opacity = "0";
         form.style.transition = "opacity 2s";

         setTimeout(() => {
            form.style = 0
         }, 2000);
      }

      return form
   }

   viewForm() {
      const form = document.querySelector('.modal')
      const personal = document.querySelector('.personal')

      personal.onclick = (e) => {
         e.stopImmediatePropagation()
         setTimeout(() => {
            form.style.opacity = "1";
            form.style.transform = "translate(0px, 0%)";
         }, 100);
         console.log("active")
      }
   }
}

const login = new Login()